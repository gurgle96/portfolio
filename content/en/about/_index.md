---
title: "About"
description: "Hi! My name is Jørgen Nilsen"
featured_image: 'images/about/solanrepair.jpg'
---

{{< figure src="/portfolio/images/about/aboutjorgen.jpg" >}}

Engineering, personal development and teamwork are my passions. My name is Jørgen Nilsen, based in Grimstad, 
Norway and currently studying for a Master's degree in Mechatronics at the University of Agder, graduating June 2021. 
I have a Bachelor's degree within Mechatronics, specialising in C/C++ programming, embedded systems, hardware and servo technology. 
Throughout my time as a student i have been engaged in student-organizations and always pursued gaining relevant experience.  
Welcome to my portfolio, please view my previous projects and send me and e-mail for further inquiries. 


