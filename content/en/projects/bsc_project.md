---
date: 2017-04-09T10:58:08-04:00
description: "UiA Mechatronics Bachelor project"
featured_image: "images/projects/Drawing3.png"
tags: ["scene"]
title: "Mechatronics Bachelor Project"
---

The final project in a three-year engineering education at the University of Agder, resulting in a Bachelor of Science (B.Sc) in Mechatronics. 
The project was to develop a electro-mechanical system for actuating the throttle valve for a Internal Combustion (IC) engine, based upon sensor inputs from the accelerator pedal. 
A similar system to what is found on all modern day cars with an interal combustion engine. 

Through many iterations of circuit designs, revisions of code and testing with hardware. The project resultet in a functional system for controlling the air-flow for the 
IC engine, with all safety functions implemented.

[GitHub project](https://github.com/align-racing-uia/AR19/tree/master/AR19_ETC)

{{< figure src="/portfolio/images/projects/bachelor_etbc.jpg" >}}

{{< figure src="/portfolio/images/projects/bachelor_bspd.jpg" >}}


