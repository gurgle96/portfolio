---
date: 2017-04-10T11:00:59-04:00
description: "Align Racing UiA 2018"
featured_image: "/images/projects/ar18ludvig_header.jpg"
tags: ["scene"]
title: "Align Racing UiA: AR18-Ludvig"
---


{{< figure src="/portfolio/images/projects/ar18ludvig_flag.jpg" >}}


The startup of Align Racing UiA

The Formula Student dream at the University in Agder started already in 2016 when two groups of students wanted to start something like this. One of the groups had good contact with the faculty, and the other had good contact with the students. Despite the interest, it would take a year before the two groups found each other’s and became one at the career day in 2017. The interest in a project like this was large. A startup group of 15-20 students from different studies was formed.

Formula Student is to operate an innovative car firm with a new prototype each year. Align thought of this as a core foundation of the organization. Because of this, it was also said from the beginning that this wasn’t only an engineer competition.


{{< figure src="/portfolio/images/projects/ar18ludvig_turning.jpg" >}}

The vision of Align Racing was carefully planned with three main points. It is an arena with a focus on personal development, for practical expertise towards study and interests, and finally, for everyone to be able to apply, no matter their study.

A large recruitment progress was started with over 180 applicants. All leaders and other member positions were filled to the brim. Research on the different parts of the organization was already started the week after the career day.

The startup group was quick to start and were because of that able to get in contact with both ION Racing from UiS and Revolve from NTNU early on. Both were very positive to get another Norwegian team into the competition and gave a lot of priceless advice. ION Racing arranged a visit and brought three Align members with them to Silverstone in 2017

Align got in contact with Tekreal, short for the faculty of technology science, early on, and they were very willing to assist economically. The positivity of Tekreal towards the project and quite a few coincidences made it so that Align Racing got an office and workshop areas in the new MIL-building

April 26th, one year before Align Racing UiA rolled out the first formula car, there was held a kick-off arrangement. With high volume, a borrowed formula car from ION Racing UiS, and video-effects, the adventure was started.

If it weren’t for the positivity of Tekreal, and the push and willpower of the startup group, Align Racing UiA would never have been where it is today.

