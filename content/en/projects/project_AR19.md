---
date: 2017-04-09T10:58:08-04:00
description: "Align Racing UiA 2019"
featured_image: "/images/projects/ar19solan_header.jpg"
tags: ["scene"]
title: "Align Racing UiA: AR19-Solan"
---

AR19-Solan is Align Racing UiA's second Formula Student car, with the larger and heavier AR18-Ludvig as its big brother. The goal of AR19-Solan was to create a platform which could be further developed through iterative development for the years to come. At the start of the year we made some key points for which properties we wanted the car to have, both in regards to its physical appearance, as well as some technical specifications like for example the dampener system, engine type, turbo etc. Here we used all our knowledge and experience from our first car, as well as some inspiration from other Formula Student teams. We aimed to show what students in an interdisciplinary project have the capabilities to do, and the final reveal took place at campus Grimstad. This was the result of eight months hard work, focus and engagement.

AR19-Solan, as it officially is called, weighs in at 243 kg, driven by a turbocharged KTM 690 engine, in a self-designed steel frame and suspension assembly.

The time approaching competitions was used for testing and optimization of the systems driver training. The goal of the testing was to reveal weaknesses with the design in dynamic circumstances, experience its handling, and to prepare for Formula Student at Silverstone in England.

Eventually it was time for the competition. A truck was loaded, and the team travelled to England. The car quickly got through the technical control and took part in all dynamic tests. The end results were a third place in cost report, a third place in the marketing competition, 12th place in 20km endurance race and an overall 13th place in the competition. We are incredibly proud of these results, and they really showed us that “hard work pays off”! 